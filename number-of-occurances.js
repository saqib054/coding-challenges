/*
	Write a function that count the numbers of occurrences of desk type.
	The function is expected to return{ sitting: 3, standing: 2 }
*/

var desks = [
	{ type: 'sitting' },
	{ type: 'standing' },
	{ type: 'sitting' },
	{ type: 'sitting' },
	{ type: 'standing' }
];

const getNumberOfOccurances = desks.reduce((occurances, desk) => {
	occurances[desk.type] ? occurances[desk.type]++ : occurances[desk.type] = 1;
	return occurances;
}, {});

console.log(getNumberOfOccurances);